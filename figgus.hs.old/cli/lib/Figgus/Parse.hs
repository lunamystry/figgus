{-# LANGUAGE GADTs             #-}
{-# LANGUAGE OverloadedStrings #-}

module Figgus.Parse where

import           Control.Monad                  ( void )
import           Data.Time
import           Data.Time.Format
import           Data.Void
import qualified Data.Text                     as T
import           Figgus.DateUtils
import           Figgus.Accounts
import           Figgus.Statements
import           Figgus.Transactions
import           Text.Megaparsec
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer    as L

type Parser = Parsec Void String

sc :: Parser ()
sc = L.space (void $ some (char ' ' <|> char '\t')) empty empty

lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc

integer :: Parser Int
integer = lexeme L.decimal

float :: Parser Float
float = L.signed sc (lexeme L.float)

strCellP :: Parser String
strCellP = lexeme (many $ noneOf [',', '\r', '\n']) <?> "string"

lineP :: Parser [String]
lineP = sepBy strCellP (char ',')

dayP :: String -> Parser Day
dayP = parseTimeM True defaultTimeLocale "%-d %B %Y"

stripQuotes :: String -> Parser String
stripQuotes = return . filter (\c -> c /= '\'')

asCents :: Float -> Parser Cents
asCents rands = return $ round $ rands * 100

ignoredLine :: Char -> Parser String
ignoredLine startChar = char startChar >> manyTill printChar eol

accountP :: Parser Account
accountP = do
  char '2'
  char ','
  accountNumber <- strCellP
  char ','
  _ <- strCellP
  char ','
  accountName <- strCellP
  eol
  return $ Account (T.pack accountName) (T.pack accountNumber)

data Metadata = Metadata
  { metaNumber         :: Int
  , metaFromDay        :: Day
  , metaToDay          :: Day
  , metaOpeningBalance :: Cents
  , metaClosingBalance :: Cents
  , metaVatPaid        :: Cents
}

metadataP :: Parser Metadata
metadataP = do
  ignoredLine '3'
  ignoredLine '3'
  ignoredLine '3'
  char '3'
  char ','
  space
  statementNumber <- integer
  space
  char ','
  fromDate <- strCellP >>= stripQuotes >>= dayP
  char ','
  toDate <- strCellP >>= stripQuotes >>= dayP
  char ','
  openingBalance <- float >>= asCents
  char ','
  closingBalance <- float >>= asCents
  char ','
  vatPaid <- float >>= asCents
  eol
  return $ Metadata statementNumber
                    fromDate
                    toDate
                    openingBalance
                    closingBalance
                    vatPaid

summaryP :: Parser [String]
summaryP = many $ ignoredLine '4'



transactionP :: StatementId -> Day -> Parser Transaction
transactionP sId startDay = do
  char '5'
  char ','
  _ <- integer
  char ','
  transactionDate <- strCellP >>= stripQuotes >>= date
  char ','
  description1 <- strCellP
  char ','
  description2 <- strCellP
  char ','
  description3 <- strCellP
  char ','
  amount <- float >>= asCents
  char ','
  _ <- float
  char ','
  _ <- strCellP
  eol
  let transactionDay = setYear (year startDay) transactionDate
  return $ Transaction
    { _transactionId          = makeTransactionId amount transactionDay
    , _transactionAmount      = amount
    , _transactionDay         = transactionDay
    , _transactionDescription = T.pack
                                $  description1
                                ++ " "
                                ++ description2
                                ++ " "
                                ++ description3
    , _transactionStatement   = sId
    }
 where
  date :: String -> Parser Day
  date = parseTimeM True defaultTimeLocale "%d %b"

transactionsP :: StatementId -> Day -> Parser [Transaction]
transactionsP sId startDay = do
  ignoredLine '5'
  ignoredLine '5'
  ignoredLine '5'
  many (transactionP sId startDay)

statementP :: Parser (Account, Statement, [Transaction])
statementP = do
  account      <- accountP
  metadata     <- metadataP
  _            <- summaryP
  transactions <- transactionsP (StatementId $ metaNumber metadata)
                                (metaFromDay metadata)
  char '6' >> char ',' >> string "'END'"
  return
    $ ( account
      , Statement { _statementNumber         = metaNumber metadata
                  , _statementFromDay        = metaFromDay metadata
                  , _statementToDay          = metaToDay metadata
                  , _statementOpeningBalance = metaOpeningBalance metadata
                  , _statementAccount = AccountId (_accountNumber account)
                  }
      , transactions
      )

parseStatement :: String -> Either String (Account, Statement, [Transaction])
parseStatement sStr = case parse statementP "" sStr of
  Left  err -> Left $ errorBundlePretty err
  Right s   -> Right s
