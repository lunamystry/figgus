{ pkgs ? import <nixpkgs> { } }:
with pkgs;
with haskellPackages;

let
  cabal2nix = src: pkgs.runCommand "cabal2nix"
    {
      buildCommand = ''
        cabal2nix file://"${builtins.filterSource (path: type: path != ".git") src}" > $out
      '';
      buildInputs = [
        pkgs.cabal2nix
      ];
    } "";
  figgus = callPackage (cabal2nix ./.) { };
in
stdenv.mkDerivation rec {
  name = "figgus-cli";
  src = ./.;
  buildInputs = [
    pkgs.zlib
    figgus
  ];
  installPhase = ''
    mkdir -p $out/
    cp -r ${figgus}/* $out
  '';
}
