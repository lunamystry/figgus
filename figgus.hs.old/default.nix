{ pkgs ? import <nixpkgs> { } }:
with pkgs;
with haskellPackages;
let
  server = import ./server/default.nix { };
  client = import ./client/default.nix { };
in
stdenv.mkDerivation rec {
  name = "figgus";
  src = ./.;
  buildInputs = [
    pkgs.zlib
    server
    client
  ];
  installPhase = ''
    mkdir -p $out/
    cp -r ${server}/bin/* $out/
    cp -r ${client}/dist/ $out/client
  '';
}
