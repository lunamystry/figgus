{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeFamilies #-}

module Main where

import           Options
import           Figgus
import           Figgus.Server
import           Database.SQLite.Simple         ( open
                                                , close
                                                , Connection
                                                )

route :: Command -> Connection -> IO ()
route (Serve  opts) conn = runServer (_soPort opts) (_soClient opts) conn
route (Tag    opts) conn = tagTransactions conn
route (Seed   opts) conn = seedTransactions conn $ _seedFile opts
route (Parse  opts) conn = addTransactions conn $ _parseFile opts
route (Totals opts) conn = countTransactions conn >>= mapM_ printTotal

initDb :: FilePath -> IO Connection
initDb dbFile = do
  putStrLn $ "Opening db file: " <> dbFile
  conn <- open dbFile
  createTables conn
  seedTags conn
  pure conn

closeDb :: Connection -> IO ()
closeDb conn = putStrLn "Closing db" >> close conn

main :: IO ()
main = do
  options <- parseCLI
  conn    <- initDb $ _optDb options
  route (_optCommand options) conn
  closeDb conn
