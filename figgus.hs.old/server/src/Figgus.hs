{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeFamilies #-}

module Figgus where

import           Control.Applicative
import           Control.Lens
import qualified Data.ByteString.Char8         as C
import qualified Data.ByteString.Lazy          as BL
import           Data.Csv
import qualified Data.Text                     as T
import           Data.Time
import qualified Data.Vector                   as V
import           Database.Beam
import           Database.Beam.Sqlite
import           Database.SQLite.Simple  hiding ( Statement )
import           Figgus.Accounts
import           Figgus.Parse
import           Figgus.Statements
import           Figgus.Tags
import           Figgus.Transactions


data FiggusDb f = FiggusDb { _figgusAccounts :: f (TableEntity AccountT)
                           , _figgusStatements :: f (TableEntity StatementT)
                           , _figgusTransactions :: f (TableEntity TransactionT)
                           , _figgusTags :: f (TableEntity TagT)
                           , _figgusTransactionTags :: f (TableEntity TransactionTagT)
                           } deriving (Generic, Database be) -- be can be Sqlite or Postgres

FiggusDb (TableLens figgusAccounts) (TableLens figgusStatements) (TableLens figgusTransactions) (TableLens figgusTags) (TableLens figgusTransactionTags)
  = dbLenses

figgusDb :: DatabaseSettings be FiggusDb
figgusDb = defaultDbSettings

createTables :: Connection -> IO ()
createTables conn = do
  -- TODO: foreign keys
  execute_
    conn
    "CREATE TABLE IF NOT EXISTS accounts (number VARCHAR PRIMARY KEY, name VARCHAR NOT NULL)"
  execute_
    conn
    "CREATE TABLE IF NOT EXISTS transactions (id VARCHAR PRIMARY KEY, amount INTEGER NOT NULL, day DATE NOT NULL, description VARCHAR NOT NULL, statement__number INTEGER NOT NULL)"
  execute_
    conn
    "CREATE TABLE IF NOT EXISTS statements (number INTEGER PRIMARY KEY, from_day DATE NOT NULL, to_day DATE NOT NULL, opening_balance INTEGER NOT NULL, account__number VARCHAR NOT NULL)"
  execute_
    conn
    "CREATE TABLE IF NOT EXISTS tags (id VARCHAR PRIMARY KEY, description VARCHAR NOT NULL)"
  execute_
    conn
    "CREATE TABLE IF NOT EXISTS transaction_tags (tag__id VARCHAR NOT NULL, transaction__id VARCHAR NOT NULL)"

upsertAccount :: Connection -> Account -> IO ()
upsertAccount conn item = do
  isIn <- isInDb
  if isIn
    then
      putStrLn $ "account: " ++ (show $ item ^. accountName) ++ " already in DB"
    else
      runBeamSqlite conn
      $ runInsert
      $ insert (figgusDb ^. figgusAccounts)
      $ insertValues [item]
 where
  itemQ = do
    acc <- all_ $ figgusDb ^. figgusAccounts
    guard_ $ acc ^. accountNumber ==. val_ (item ^. accountNumber)
    pure acc
  isInDb = runBeamSqlite conn $ do
    xs <- runSelectReturningList $ select itemQ
    return $ length xs > 0

upsertStatement :: Connection -> Statement -> IO ()
upsertStatement conn item = do
  isIn <- isInDb
  if isIn
    then
      putStrLn
      $  "statement: "
      ++ (show $ item ^. statementNumber)
      ++ " already in DB"
    else
      runBeamSqlite conn
      $ runInsert
      $ insert (figgusDb ^. figgusStatements)
      $ insertValues [item]
 where
  itemQ = do
    acc <- all_ $ figgusDb ^. figgusStatements
    guard_ $ acc ^. statementNumber ==. val_ (item ^. statementNumber)
    pure acc
  isInDb = runBeamSqlite conn $ do
    xs <- runSelectReturningList $ select itemQ
    return $ length xs > 0

upsertTransaction :: Connection -> Transaction -> IO ()
upsertTransaction conn item = do
  isIn <- isInDb
  if isIn
    then return ()
    else
      runBeamSqlite conn
      $ runInsert
      $ insert (figgusDb ^. figgusTransactions)
      $ insertValues [item]
 where
  itemQ = do
    acc <- all_ $ figgusDb ^. figgusTransactions
    guard_ $ acc ^. transactionId ==. val_ (item ^. transactionId)
    pure acc
  isInDb = runBeamSqlite conn $ do
    xs <- runSelectReturningList $ select itemQ
    return $ length xs > 0

upsertTag :: Connection -> Tag -> IO ()
upsertTag conn item = do
  isIn <- isInDb
  if isIn
    then return ()
    else
      runBeamSqlite conn
      $ runInsert
      $ insert (figgusDb ^. figgusTags)
      $ insertValues [item]
 where
  itemQ = do
    acc <- all_ $ figgusDb ^. figgusTags
    guard_ $ acc ^. tagId ==. val_ (item ^. tagId)
    pure acc
  isInDb = runBeamSqlite conn $ do
    xs <- runSelectReturningList $ select itemQ
    return $ length xs > 0

getTags :: Connection -> IO [T.Text]
getTags conn = runBeamSqlite conn $ do
  runSelectReturningList $ select $ do
    tags <- all_ $ figgusDb ^. figgusTags
    pure $ tags ^. tagId

upsertTransactionTag :: Connection -> TransactionTag -> IO ()
upsertTransactionTag conn item = do
  isIn <- isInDb
  if isIn
    then return ()
    else
      runBeamSqlite conn
      $ runInsert
      $ insert (figgusDb ^. figgusTransactionTags)
      $ insertValues [item]
 where
  itemQ = do
    acc <- all_ $ figgusDb ^. figgusTransactionTags
    guard_
      $   acc
      ^.  ttTransaction
      ==. val_ (item ^. ttTransaction)
      &&. acc
      ^.  ttTag
      ==. val_ (item ^. ttTag)
    pure acc
  isInDb = runBeamSqlite conn $ do
    xs <- runSelectReturningList $ select itemQ
    return $ length xs > 0

countTransactions :: Connection -> IO [(Tag, Maybe Int)]
countTransactions conn = runBeamSqlite conn $ do
  runSelectReturningList $ select $ do
    aggregate_
        (\(tag, transaction) ->
          (group_ tag, sum_ (transaction ^. transactionAmount))
        )
      $ do
          tTag        <- all_ (figgusDb ^. figgusTransactionTags)
          tag         <- related_ (figgusDb ^. figgusTags) (_ttTag tTag)
          transaction <- related_ (figgusDb ^. figgusTransactions)
                                  (_ttTransaction tTag)
          pure (tag, transaction)

getTransactions :: Connection -> IO [Transaction]
getTransactions conn =
  runBeamSqlite conn $ runSelectReturningList $ select $ all_
    (figgusDb ^. figgusTransactions)

upsert :: Connection -> (Account, Statement, [Transaction]) -> IO ()
upsert conn (account, statement, transactions) = do
  upsertAccount conn account
  upsertStatement conn statement
  mapM_ (upsertTransaction conn) transactions

readStatement
  :: String -> IO (Either String (Account, Statement, [Transaction]))
readStatement = return . parseStatement

seedTags :: Connection -> IO ()
seedTags conn = do
  let tags =
        [ "Leonomi"
        , "Family"
        , "Groceries"
        , "Transport"
        , "Savings"
        , "Bank charges"
        , "Debt"
        , "Eat out"
        , "Internet"
        , "Well being"
        , "Fun"
        ]
  let pairs = zip tags $ (T.replace " " "-") . T.toLower <$> tags
  mapM_ (upsertTag conn) $ [ Tag (snd t) (fst t) | t <- pairs ]

promptForNumber :: String -> IO Int
promptForNumber p = do
  putStrLn p
  strNum <- getLine
  case reads strNum of
    [(val, "")] -> pure val
    _           -> promptForNumber $ "(must be a number) " ++ p

tagTransactions :: Connection -> IO ()
tagTransactions conn = do
  tags            <- getTags conn
  -- TODO: only get untagged transactions
  transactions    <- getTransactions conn
  transactionTags <- mapM (tagTransaction tags) transactions
  mapM_ (upsertTransactionTag conn) transactionTags
 where
  tagTransaction tags t = do
    putStrLn $ showTransaction t
    n <- promptForNumber $ showTags tags
    return $ TransactionTag (TagId $ tags !! n)
                            (TransactionId $ t ^. transactionId)
  showTransaction :: Transaction -> String
  showTransaction t =
    strRands (t ^. transactionAmount)
      ++ " "
      ++ T.unpack (t ^. transactionDescription)
      ++ " "
      ++ show (t ^. transactionDay)
      ++ " "
  showTags :: [T.Text] -> String
  showTags ts =
    T.unpack
      $ T.intercalate ", "
      $ map (\i -> T.pack $ show (snd i) ++ " - " ++ T.unpack (fst i))
      $ zip ts [0 ..]

printTotal :: (Tag, Maybe Int) -> IO ()
printTotal (tag, total) =
  putStrLn $ T.unpack (tag ^. tagDescription) ++ " - " ++ maybeShow total
 where
  maybeShow t = case t of
    Just num -> strRands num
    Nothing  -> "nothing"

seedTransactions :: Connection -> FilePath -> IO ()
seedTransactions conn csvFilename = do
  csvData <- BL.readFile csvFilename
  case decode NoHeader csvData of
    Left err -> putStrLn err
    Right v ->
      V.forM_ v
        $ \(stId :: Int, day :: Day, rands :: Float, balance :: Int, description :: T.Text) ->
            upsertTransaction conn $ Transaction
              { _transactionId          = (id rands day)
              , _transactionAmount      = (asCents rands)
              , _transactionDay         = day
              , _transactionDescription = description
              , _transactionStatement   = (StatementId stId)
              }
     where
      asCents r = round $ r * 100
      id r d = makeTransactionId (asCents r) d

instance FromField Day where
  parseField s = parseTimeM True defaultTimeLocale "%Y/%m/%d" $ C.unpack s

addTransactions :: Connection -> FilePath -> IO ()
addTransactions conn f =
  readFile f >>= readStatement >>= either putStrLn (upsert conn)
