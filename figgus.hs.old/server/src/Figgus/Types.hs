{-# LANGUAGE OverloadedStrings #-}

module Figgus.Types where

import           Data.Time                      ( Day )

type AccountNumber = String
type Cents = Integer
type Description = String

data Account = Account
  { accountName   :: String
  , accountNumber :: AccountNumber
  } deriving (Eq, Show)

data Transaction = Transaction
  { transactionId        :: String
  , amount               :: Cents
  , transactionDate      :: Day
  , description          :: Description
  , bankDescription      :: Description
  , recepientDescription :: Description
  } deriving (Eq, Show)


data Statement = Statement
  { statementNumber :: Integer
  , fromDay         :: Day
  , toDay           :: Day
  , openingBalance  :: Cents
  , account         :: Account
  , transactions    :: [Transaction]
  } deriving (Eq, Show)
