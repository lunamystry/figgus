{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImpredicativeTypes #-}

module Figgus.Statements where

import           Database.Beam
import           Database.Beam.Sqlite
import           Database.SQLite.Simple  hiding ( Statement )
import           Data.Aeson.Types
import           Data.Text                      ( Text )
import           Data.Time                      ( Day )
import           Figgus.Accounts

data StatementT f = Statement
  { _statementNumber          :: C f Int
  , _statementFromDay         :: C f Day
  , _statementToDay           :: C f Day
  , _statementOpeningBalance  :: C f Int
  , _statementAccount         :: PrimaryKey AccountT f
  } deriving (Generic, Beamable)

type Statement = StatementT Identity
type StatementId = PrimaryKey StatementT Identity
deriving instance Show Statement
deriving instance Eq Statement
-- This is for when it is used in other tables
deriving instance Show (PrimaryKey StatementT Identity)
deriving instance Eq (PrimaryKey StatementT Identity)
deriving instance ToJSON (PrimaryKey StatementT Identity)

instance Table StatementT where
  data PrimaryKey StatementT f = StatementId (C f Int) deriving (Generic, Beamable)
  primaryKey = StatementId . _statementNumber

Statement (LensFor statementNumber) (LensFor statementFromDay) (LensFor statementToDay) (LensFor statementOpeningBalance) (AccountId (LensFor statementAccount))
  = tableLenses
