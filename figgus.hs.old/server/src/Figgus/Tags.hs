{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImpredicativeTypes #-}

module Figgus.Tags where

import           Database.Beam
import           Database.Beam.Sqlite
import           Database.SQLite.Simple
import           Data.Text                      ( Text )
import           Data.Time                      ( Day )
import           Figgus.Transactions

data TagT f = Tag
  { _tagId          :: C f Text
  , _tagDescription :: C f Text
  } deriving (Generic, Beamable)

type Tag = TagT Identity
type TagId = PrimaryKey TagT Identity
deriving instance Show (PrimaryKey TagT Identity)
deriving instance Eq (PrimaryKey TagT Identity)
deriving instance Show Tag
deriving instance Eq Tag

instance Table TagT where
  data PrimaryKey TagT f = TagId (C f Text) deriving (Generic, Beamable)
  primaryKey = TagId . _tagId

Tag (LensFor tagId) (LensFor tagDescription) = tableLenses

-- many-to-many transcation and tags


data TransactionTagT f = TransactionTag
  { _ttTag         :: PrimaryKey TagT f
  , _ttTransaction :: PrimaryKey TransactionT f
  } deriving (Generic, Beamable)

type TransactionTag = TransactionTagT Identity
deriving instance Show TransactionTag

instance Table TransactionTagT where
  data PrimaryKey TransactionTagT f = TransactionTagId (PrimaryKey TagT f) (PrimaryKey TransactionT f)
                                deriving (Generic, Beamable)
  primaryKey = TransactionTagId <$> _ttTag <*> _ttTransaction

TransactionTag (TagId (LensFor ttTag)) (TransactionId (LensFor ttTransaction))
  = tableLenses
