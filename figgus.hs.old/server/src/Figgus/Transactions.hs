{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImpredicativeTypes #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Figgus.Transactions where

import           Database.Beam
import           Database.Beam.Sqlite
import           Database.SQLite.Simple
import           Data.Aeson.Types
import           Data.Text                      ( Text
                                                , pack
                                                , unpack
                                                )
import           Data.Time                      ( Day )
import           Figgus.Statements

type Cents = Int
type Description = Text

data TransactionT f = Transaction
  { _transactionId                   :: C f Text
  , _transactionAmount               :: C f Cents
  , _transactionDay                  :: C f Day
  , _transactionDescription          :: C f Description
  , _transactionStatement            :: PrimaryKey StatementT f
  } deriving (Generic, Beamable)

asRands :: Int -> Double
asRands cents = (1.0 * fromIntegral cents) / 100

strRands :: Int -> String
strRands cents = "R" ++ (show $ asRands cents)

padL :: a -> Int -> [a] -> [a]
padL p s l | length l >= s = l
           | otherwise     = replicate (s - length l) p ++ l
{-# INLINABLE padL #-}

type Transaction = TransactionT Identity
type TransactionId = PrimaryKey TransactionT Identity
instance Show Transaction where
  show (Transaction id cents day desc st) =
    padL ' ' 9 (strRands cents) ++ "\t" ++ show day ++ "\t" ++ unpack desc
instance ToJSON Transaction where
  toJSON (Transaction i a d de s) = object
    [ "id" .= i
    , "rands" .= asRands a
    , "day" .= d
    , "description" .= de
    , "statementId" .= s
    ]

deriving instance Eq Transaction
-- This is for when it is used in other tables
deriving instance Show (PrimaryKey TransactionT Identity)
deriving instance Eq (PrimaryKey TransactionT Identity)
deriving instance ToJSON (PrimaryKey TransactionT Identity)

instance Table TransactionT where
  data PrimaryKey TransactionT f = TransactionId (C f Text) deriving (Generic, Beamable)
  primaryKey = TransactionId . _transactionId

Transaction (LensFor transactionId) (LensFor transactionAmount) (LensFor transactionDay) (LensFor transactionDescription) (StatementId (LensFor transactionStatement))
  = tableLenses

makeTransactionId :: Int -> Day -> Text
makeTransactionId a d = pack $ show a <> "-" <> show d
