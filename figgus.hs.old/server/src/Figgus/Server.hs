{-# LANGUAGE DataKinds            #-}
{-# LANGUAGE OverloadedStrings    #-}
{-# LANGUAGE TypeOperators        #-}

module Figgus.Server where

import Control.Monad.IO.Class (liftIO)
import Control.Concurrent
import Data.Monoid ((<>))
import Data.Text.Lazy (Text, pack)
import Data.Text.Lazy.Encoding (encodeUtf8)
import Figgus
import Figgus.Transactions
import Network.HTTP.Media ((//), (/:))
import Network.Wai
import Network.Wai.Application.Static
import Network.Wai.Handler.Warp (run)
import Network.Wai.Middleware.RequestLogger
import Network.Wai.Middleware.Static
import Options
import Servant
import WaiAppStatic.Types

type Message = String;

type API = "api" :> ReqBody '[PlainText] Message :> Post '[JSON] NoContent
      :<|> "api" :> "transactions" :> Get '[JSON] [Transaction]
      :<|> Raw

server :: String -> Connection -> Server API
server clientDir conn = createMessage
              :<|> transactions conn
              :<|> serverClient clientDir
              where
                createMessage :: Message -> Handler NoContent
                createMessage msg = pure NoContent
                transactions :: Connection ->  Handler [Transaction]
                transactions conn = liftIO $ getTransactions conn >>= pure
                serverClient :: String -> Server Raw
                serverClient clientDir =
                    serveDirectoryWith (mySettings clientDir)
                      where
                        mySettings root = ds { ssLookupFile = lookupFile }
                          where
                          ds = defaultFileServerSettings root
                          lookupFile p = ssLookupFile ds p >>= maybeLookupIndex [unsafeToPiece "index.html"]
                          maybeLookupIndex _ (LRFile f)     = return $ LRFile f
                          maybeLookupIndex _ (LRFolder f)   = return $ LRFolder f
                          maybeLookupIndex index LRNotFound = ssLookupFile ds index

api :: Proxy API
api = Proxy

runServer :: Int -> FilePath -> Connection -> IO ()
runServer port clientDir conn = do
  putStrLn $ "Running on :" <> show port <> " serving client: " <> clientDir
  run port (serve api $ server clientDir conn)
