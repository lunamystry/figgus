module Figgus.DateUtils where

import           Data.Time
import           Data.Time.Calendar

sel1 :: (a, b, c) -> a
sel1 (a, b, c) = a

sel2 :: (a, b, c) -> b
sel2 (a, b, c) = b

sel3 :: (a, b, c) -> c
sel3 (a, b, c) = c

year :: Day -> Integer
year = sel1 . toGregorian

month :: Day -> Int
month = sel2 . toGregorian

day :: Day -> Int
day = sel3 . toGregorian

setYear :: Integer -> Day -> Day
setYear y d = fromGregorian y (month d) (day d)
