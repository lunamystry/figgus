{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ImpredicativeTypes #-}

module Figgus.Accounts where

import           Database.Beam
import           Database.Beam.Sqlite
import           Database.SQLite.Simple
import           Data.Text                      ( Text )

type AccountNumber = Text
data AccountT f = Account
  { _accountName   :: C f Text
  , _accountNumber :: C f AccountNumber
  } deriving (Generic, Beamable)

type Account = AccountT Identity
type AccountId = PrimaryKey AccountT Identity
deriving instance Show Account
deriving instance Eq Account
-- This is for when the Account is used in another Table
deriving instance Show (PrimaryKey AccountT Identity)
deriving instance Eq (PrimaryKey AccountT Identity)

instance Table AccountT where
  data PrimaryKey AccountT f = AccountId (C f Text) deriving (Generic, Beamable)
  primaryKey = AccountId . _accountNumber

Account (LensFor accountName) (LensFor accountNumber) = tableLenses
