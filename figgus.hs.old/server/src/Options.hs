{-# LANGUAGE ConstraintKinds            #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}

module Options
  ( Options(..)
  , Command(..)
  , ServeOptions(..)
  , TagOptions(..)
  , SeedOptions(..)
  , ParseOptions(..)
  , TotalsOptions(..)
  , parseCLI
  )
where

import           Data.Semigroup                 ( (<>) )
import           Options.Applicative
import           Options.Applicative.Types

{-|
figgus [--db DATABASEFILE] [--debug [LEVEL] info (info|error|warn|debug)] COMMAND
figgus serve [--client CLIENTDIRECTORY data/client] [--host HOST localhost] [--port PORT 8080]
figgus tag [--start-date STARTDATE] [--end-date ENDDATE]
figgus seed [--file FILE data/seed.csv]
figgus parse FILE
-}

data Options = Options
  { _optDb      :: FilePath
  , _optDebug   :: Debug
  , _optCommand :: Command
  }

data Debug = InfoLevel
           | ErrorLevel
           | WarnLevel
           | DebugLevel

instance Show Debug where
  show InfoLevel  = "info"
  show ErrorLevel = "error"
  show WarnLevel  = "warn"
  show DebugLevel = "debug"

data Command = Serve ServeOptions
             | Tag TagOptions
             | Seed SeedOptions
             | Parse ParseOptions
             | Totals TotalsOptions

data ServeOptions = ServeOptions
  { _soClient :: FilePath
  , _soHost   :: Maybe String
  , _soPort   :: Int
  }

data TagOptions = TagOptions
  -- TODO: use Day instead of String
  { _tagStart :: Maybe String
  , _tagEnd   :: Maybe String
  }

data SeedOptions = SeedOptions { _seedFile :: FilePath }
data ParseOptions = ParseOptions { _parseFile :: FilePath }

data TotalsOptions = TotalsOptions
  -- TODO: use Day instead of String
  { _totalsStart :: Maybe String
  , _totalsEnd   :: Maybe String
  }

options :: Parser Options
options = Options <$> parseDb <*> parseDebug <*> parseCommand
 where
  parseDb :: Parser FilePath
  parseDb =
    strOption
      $  short 'd'
      <> long "database"
      <> metavar "DB_FILE"
      <> help "SQLite file"
      <> value "data/figgus.sqlite"
      <> showDefault
  parseDebug :: Parser Debug
  parseDebug =
    (option debugLevel)
      $  short 'v'
      <> long "debug"
      <> metavar "DEBUG_LEVEL"
      <> help "The debug level"
      <> value InfoLevel
      <> showDefault
  parseCommand :: Parser Command
  parseCommand =
    hsubparser
      $  command "serve" (info serveOptions (progDesc "Start the server"))
      <> command "tag"   (info tagOptions (progDesc "Tag transactions"))
      <> command "seed"  (info seedOptions (progDesc "Seed the database"))
      <> command
           "parse"
           (info parseOptions (progDesc "Parse and save a csv statement"))
      <> command
           "totals"
           (info totalsOptions (progDesc "Calculate the transactions totals"))

debugLevel :: ReadM Debug
debugLevel = readerAsk >>= debugFromString
 where
  debugFromString :: String -> ReadM Debug
  debugFromString "error" = pure ErrorLevel
  debugFromString "info"  = pure InfoLevel
  debugFromString "warn"  = pure WarnLevel
  debugFromString "debug" = pure DebugLevel
  debugFromString s       = readerError $ "Unknown level: " <> s

serveOptions :: Parser Command
serveOptions =
  Serve <$> (ServeOptions <$> parseClient <*> parseHost <*> parsePort)
 where
  parseClient :: Parser FilePath
  parseClient =
    strOption
      $  short 'c'
      <> long "client"
      <> metavar "CLIENT_DIRECTORY"
      <> help "The client dist directory"
      <> value "data/client"
      <> showDefault
  parseHost :: Parser (Maybe String)
  parseHost =
    optional
      $  strOption
      $  short 'h'
      <> long "host"
      <> metavar "HOST"
      <> help "The host IP address to run from"
      <> value "localhost"
      <> showDefault
  parsePort :: Parser Int
  parsePort =
    option auto
      $  short 'p'
      <> long "port"
      <> metavar "PORT"
      <> help "The port to run the server from"
      <> value 8080
      <> showDefault

tagOptions :: Parser Command
tagOptions = Tag <$> (TagOptions <$> parseStart <*> parseEnd)
 where
  parseStart :: Parser (Maybe String)
  parseStart =
    optional
      $  strOption
      $  short 's'
      <> long "start"
      <> metavar "START_DATE"
      <> help "Date to start tagging from"
  parseEnd :: Parser (Maybe String)
  parseEnd =
    optional $ strOption $ short 'e' <> long "end" <> metavar "END_DATE" <> help
      "Date to stop tagging from"

seedOptions :: Parser Command
seedOptions = Seed <$> (SeedOptions <$> parseSeed)
 where
  parseSeed :: Parser FilePath
  parseSeed =
    strOption
      $  short 'f'
      <> long "file"
      <> metavar "SEED_FILE"
      <> help "A CSV file containing seed data"
      <> value "data/seed.csv"
      <> showDefault

parseOptions :: Parser Command
parseOptions = Parse <$> (ParseOptions <$> parseFile)
 where
  parseFile :: Parser FilePath
  parseFile =
    argument str $ metavar "FILE" <> help "A CSV file containing file data"

totalsOptions :: Parser Command
totalsOptions = Totals <$> (TotalsOptions <$> parseFrom <*> parseTo)
 where
  parseFrom :: Parser (Maybe String)
  parseFrom =
    optional
      $  strOption
      $  short 'f'
      <> long "from"
      <> metavar "FROM_DATE"
      <> help "First date for calculating totals"
  parseTo :: Parser (Maybe String)
  parseTo =
    optional $ strOption $ short 't' <> long "to" <> metavar "TO_DATE" <> help
      "Last date for calculating totals"

parseCLI :: IO Options
parseCLI = execParser $ info (helper <*> options) $ header "Figgus"
