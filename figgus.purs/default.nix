{ pkgs ? import <nixpkgs> {} }:
with pkgs;

mkYarnPackage rec {
  name = "figgus-client";
  src = ./.;
  packageJSON = ./package.json;
  yarnLock = ./yarn.lock;
  extraBuildInputs = [ pkgs.purescript ];
  distPhase = ''
    # pack command ignores cwd option
    rm -f .yarnrc
    cd $out/libexec/${name}/deps/${name}
    yarn build --offline 
    cp --recursive dist $out/ 
  '';
}
