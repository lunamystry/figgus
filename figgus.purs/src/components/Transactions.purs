module Transactions where

import Prelude

import Affjax as AX
import Affjax.ResponseFormat as ResponseFormat
import Data.Argonaut.Core as J
import Data.Argonaut.Decode as JD
import Data.Either (Either(..))
import Data.List (List)
import Effect.Aff (Aff)

type Transaction = { id :: String
                   , rands :: Number
                   , day :: String
                   , description :: String
                   , statementId :: Number
                   }
type Transactions = List Transaction

decodeTransaction :: J.Json -> Either String (List Transaction)
decodeTransaction = JD.decodeJson

getTransactions :: Aff (Either String Transactions)
getTransactions = do
  result <- AX.get ResponseFormat.json "/api/transactions"
  case result of
    Left err -> pure $ Left $ AX.printError err
    Right res -> pure $ decodeTransaction res.body 
