module HelloWorld where

import Prelude

import Affjax as AX
import Affjax.ResponseFormat as ResponseFormat
import Data.Argonaut.Core as J
import Data.Argonaut.Decode as JD
import Data.Either (Either(..))
import Data.List (List, intercalate) 
import Effect (Effect)
import Effect.Aff (launchAff)
import Effect.Class.Console (log)

greet :: String -> String
greet name = "Hello, " <> name <> "!"

type Todo = { userId :: Int
            , id :: Int
            , title :: String
            , completed :: Boolean
            }

decodeTodo :: J.Json -> Either String (List Todo)
decodeTodo = JD.decodeJson

main :: Effect Unit
main = void $ launchAff $ do
  result <- AX.get ResponseFormat.json "https://localhost:8000/api/transactions"
  case result of
    Left err -> log $ "FAILED: " <> AX.printError err
    Right res -> log $ "SUCCEEDED: " <> case decodeTodo res.body of
                                             Left err -> err
                                             Right xs -> intercalate ", " $ map show xs
