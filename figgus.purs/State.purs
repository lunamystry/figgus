module State where

import Prelude

import Affjax as AX
import Affjax.ResponseFormat as ResponseFormat
import Effect (Effect)
import Effect.Aff (launchAff)
import Effect.Class.Console (log)

type Transaction = { id :: String }

type State = { transactions :: List Transaction }

defaultState :: State
defaultState = State [Transaction "1"]

init :: Effect Unit
init = void $ launchAff $ do
  result <- AX.get ResponseFormat.json "/api/transactions"
  case result of
    Left err -> log $ "FAILED: " <> AX.printError err
    Right res -> log $ "SUCCEEDED: " <> case decodeTodo res.body of
                                             Left err -> err
                                             Right xs -> intercalate ", " $ map show xs

