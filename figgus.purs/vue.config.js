const path = require('path');

// vue.config.js
module.exports = {
    chainWebpack: config => {
        config
            .plugin('html')
            .tap((args) => {
                args[0].title = 'Figgus';
                return args;
            });
        // Purescript Loader
        config.module
            .rule('purescript')
            .test(/\.purs$/)
            .use('purs-loader')
            .loader('purs-loader')
            .tap(options => ({
                src: [
                    path.join('src', '**', '*.purs'),
                    path.join('bower_components', '**', 'src', '**', '*.purs'),
                ]
            }))
    }
}
