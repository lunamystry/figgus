{
  description = "Figgus - Because I happen to be a kick-ass accountant!";

  inputs = {
    haskellNix.url = "github:input-output-hk/haskell.nix";
    nixpkgs.follows = "haskellNix/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, haskellNix }: 
    flake-utils.lib.eachDefaultSystem
      (system:
      let 
        overlays = [ haskellNix.overlay
        (final: prev: {
          # This overlay adds our project to pkgs
          figgus =
            final.haskell-nix.project' {
              src = ./.;
              compiler-nix-name = "ghc8104";
            };
          })
        ];
        pkgs = import nixpkgs { inherit system overlays; };
        flake = pkgs.figgus.flake {};
      in flake // {
        defaultPackage = flake.packages."figgus:exe:figgus";
        # This is used by `nix develop .` to open a shell for use with
        # `cabal`, `hlint` and `haskell-language-server`
        devShell = pkgs.figgus.shellFor {
          tools = {
            cabal = "latest";
            hlint = "latest";
            haskell-language-server = "latest";
          };
        };
      }
  );
}
